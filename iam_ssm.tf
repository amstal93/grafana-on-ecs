data "aws_iam_policy_document" "ssm_policy" {
  statement {
    actions = [
      "ssm:GetParameters",
      "kms:Decrypt"
    ]
    resources = [
      "arn:aws:ssm:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:parameter/${var.app_name}/grafana/*",
      "arn:aws:kms:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:key/${aws_kms_key.this.id}"
    ]
  }
}

resource "aws_iam_policy" "ecs_ssm_policy" {
  name   = "${var.app_name}-grafana-ssm-policy"
  policy = data.aws_iam_policy_document.ssm_policy.json
}
